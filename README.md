MPRIS2 command-line client
==========================

Many media players used on the modern Linux desktop systems can be controlled
using the [MPRIS2 protocol][MPRIS]. This tool allows you to get status of the
player and control it using simple command-line interface.


Usage
-----

This tool is mostly self-documenting. Running `mpris2` command will provide you
an overview of features. Not all features can be used with all players and some
are available only in specific situations specified by the player.

The `mpris2` command takes a *subcommand* as the first positional argument. The
subcommand specified which action should be performed.

Multiple players can be running on the same system. You can get list of
identifiers that can be used to refer the currently running players by executing
`mpris2 players`. (This list contains also players that are not running, but can
be automatically started using D-Bus activation.)

Use the option `-p <player_id>` before the subcommand name to specify which
player should be controlled. When this options is not specified, some player
will be chosen arbitrarily by the tool. (This is usually useful only when only
one player is running.)

When multiple instances of one player application are running, the player ID
itself can be stil ambiguous. To specify which instance of the player should be
controlled, player instance ID can be used. You can get list of player instance
IDs by running `mpris2 players -i`. The instance ID is in format
`player_id.some_text`, where `some_text` is instance identifier assigned by the
player application.


[MPRIS]: https://specifications.freedesktop.org/mpris-spec/latest/
