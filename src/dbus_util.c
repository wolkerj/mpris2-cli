/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "dbus_util.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <gio/gio.h>

#include "error.h"
#include "exit_codes.h"

GDBusConnection *dbus_conn = NULL;
char const *player_bus_name = NULL;

void require_dbus(void)
{
	GError *error = NULL;

	if (dbus_conn != NULL) return;
	dbus_conn = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &error);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to get the session bus");
}

struct player_list player_list = { 0 };

void require_player_list(void)
{
	GError *error = NULL;

	if (player_list.player_buses != NULL) return;

	require_dbus();

	/* Get list of all D-Bus buses. */
	GVariantType *reply_type = g_variant_type_new("(as)");
	GVariant *reply =
		g_dbus_connection_call_sync(
			dbus_conn,
			"org.freedesktop.DBus",
			"/org/freedesktop/DBus",
			"org.freedesktop.DBus", "ListNames",
			/* params */ NULL,
			reply_type,
			G_DBUS_CALL_FLAGS_NONE,
			/* timeout */ -1 /* default */,
			NULL, &error);
	g_variant_type_free(reply_type);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to enumerate D-Bus buses");

	size_t bus_list_len;
	player_list.player_buses =
			g_variant_get_strv(
				g_variant_get_child_value(reply, 0)
				, &bus_list_len),
	player_list.len = 0;

	/* Filter out bus names that are not related to MPRIS2. */
	for (size_t name_n = 0;
	     name_n < bus_list_len;
	     name_n++) {
		char const *name = player_list.player_buses[name_n];

		if (strncmp("org.mpris.MediaPlayer2.", name, 23) == 0) {
			/* Shift the entry to the start. */
			player_list.player_buses[player_list.len++] =
				player_list.player_buses[name_n];
		}
	}
}

static void exit_invalid_player_id(char const *player_id)
{
	fprintf(stderr,
	        "mpris2: Invalid player ID: “%s”\n"
	        "mpris2: Player ID must not contain a dot and not be empty,\n"
	        "mpris2: to specify instance ID, use “player.instance”.\n",
	        player_id);
	exit(MPRIS2_EXIT_INVALID_USAGE);
}

static void exit_player_not_found(char const *player_id)
{
	fprintf(stderr,
	        "mpris2: Invalid player ID: “%s”\n"
	        "mpris2: The player was not found.\n",
	        player_id);
	exit(MPRIS2_EXIT_NO_PLAYER);
}

void set_player_id(char const *player_id)
{
	/* Check that the player ID has at most one dot. */
	char const *first_dot = strchr(player_id, '.');
	if (first_dot != NULL && strchr(first_dot + 1, '.') != NULL) {
		exit_invalid_player_id(player_id);
	}

	/* The player ID must not be empty. */
	if (*player_id == '\0') {
		exit_invalid_player_id(player_id);
	}

	if (first_dot == NULL) { /* We have player ID without instance ID. */
		/* Find the first matching player. */
		size_t player_id_len = strlen(player_id);

		require_player_list();
		for (size_t player_n = 0;
		     player_n < player_list.len;
		     player_n++) {
			char const *bus_name =
				player_list.player_buses[player_n];
			char const *player2_id =
				bus_name + 23 /* len org.mpris.MediaPlayer2 */;

			char const *first_dot_in_matched =
				strchrnul(player2_id, '.');
			size_t player2_id_len = first_dot_in_matched - player2_id;

			if (player2_id_len == player_id_len
			    && strncmp(player2_id, player_id, player_id_len)
			    == 0) {
				player_bus_name = bus_name;
				return;
			}
		}

		exit_player_not_found(player_id);
	}
		

	/* We don't do further checks, GLib will report error in other cases and
	 * we just report that as an D-Bus error. Just like if that player does
	 * not exist. */

	/* Make the full bus name string. */
	char *name = malloc(strlen(player_id) + 24);
	player_bus_name = name;
	strcpy(name, "org.mpris.MediaPlayer2.");
	memcpy(name + 23, player_id, strlen(player_id) + 1);
}

void use_first_player(void)
{			
	require_player_list();

	if (player_list.len == 0) {
		fputs("mpris2: No player to control.\n", stderr);
		exit(MPRIS2_EXIT_NO_PLAYER);
	}

	player_bus_name = player_list.player_buses[0];
}

GVariant *dbus_get_property(char const *bus_name,
                            char const *object_name,
                            char const *interface_name,
                            char const *property_name,
                            GDBusCallFlags flags,
                            GError **error)
{
	GVariantType *reply_type = g_variant_type_new("(v)");

	/* I hate D-Bus properties. */
	GVariant *reply = g_dbus_connection_call_sync(
		dbus_conn,
		bus_name,
		object_name,
		"org.freedesktop.DBus.Properties",
		"Get",
		g_variant_new("(ss)",
		              interface_name,
		              property_name),
		reply_type,
		flags,
		/* timeout */ -1 /* default */,
		NULL, error);
	if (*error != NULL) return false;

	g_variant_type_free(reply_type);
	GVariant *result = g_variant_get_child_value(
		g_variant_get_child_value(reply, 0), 0);
	g_variant_ref(result);
	g_variant_unref(reply);

	return result;
}

bool dbus_get_bool_property(char const *bus_name,
                            char const *object_name,
                            char const *interface_name,
                            char const *property_name,
                            GDBusCallFlags flags,
                            GError **error)
{
	GVariant *reply = dbus_get_property(bus_name,
	                                    object_name,
	                                    interface_name,
	                                    property_name,
	                                    flags, error);
	if (*error) return false;
	bool result = g_variant_get_boolean(reply);
	g_variant_unref(reply);
	return result;
}

void dbus_set_bool_property(char const *bus_name,
                            char const *object_name,
                            char const *interface_name,
                            char const *property_name,
                            bool new_state,
                            GDBusCallFlags flags,
                            GError **error)
{
	/* I hate D-Bus properties here even more. */
	GVariant *reply = g_dbus_connection_call_sync(
		dbus_conn,
		bus_name,
		object_name,
		"org.freedesktop.DBus.Properties",
		"Set",
		g_variant_new("(ssv)",
		              interface_name,
		              property_name,
		              g_variant_new("b", new_state)),
		/* reply type */ NULL,
		flags,
		/* timeout */ -1 /* default */,
		NULL, error);
	if (*error != NULL) return;

	g_variant_unref(reply);
}

double dbus_get_double_property(char const *bus_name,
                                char const *object_name,
                                char const *interface_name,
                                char const *property_name,
                                GDBusCallFlags flags,
                                GError **error)
{
	GVariant *reply = dbus_get_property(bus_name,
	                                    object_name,
	                                    interface_name,
	                                    property_name,
	                                    flags, error);
	if (*error) return false;
	double result = g_variant_get_double(reply);
	g_variant_unref(reply);
	return result;
}

void dbus_set_string_property(char const *bus_name,
                              char const *object_name,
                              char const *interface_name,
                              char const *property_name,
                              char const *new_state,
                              GDBusCallFlags flags,
                              GError **error)
{
	/* D-Bus properties are quite nice. */
	GVariant *reply = g_dbus_connection_call_sync(
		dbus_conn,
		bus_name,
		object_name,
		"org.freedesktop.DBus.Properties",
		"Set",
		g_variant_new("(ssv)",
		              interface_name,
		              property_name,
		              g_variant_new("s", new_state)),
		/* reply type */ NULL,
		flags,
		/* timeout */ -1 /* default */,
		NULL, error);
	if (*error != NULL) return;

	g_variant_unref(reply);
}
