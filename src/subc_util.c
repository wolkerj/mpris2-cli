/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subc_util.h"

#include <stdio.h>
#include <stdlib.h>

#include "exit_codes.h"
#include "subcommands.h"

void exit_subc_invalid_usage(struct subc *subc, char const *msg)
{
	if (msg != NULL) {
		fprintf(stderr, "mpris2: %s\n", msg);
	}
	char const *subc_usage = subc->usage;
	if (subc_usage == NULL) {
		subc_usage = " (no options)";
	}
	fprintf(stderr,
	        "mpris2: Invalid usage\n"
	        "Usage:  mpris2 %s %s\n"
	        "        mpris2 <subcommand> [args...]\n",
	        subc->name, subc_usage);
	exit(MPRIS2_EXIT_INVALID_USAGE);
}

