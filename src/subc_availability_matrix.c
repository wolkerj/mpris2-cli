/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "exit_codes.h"
#include "subc_util.h"
#include "subcommands.h"

int subc_availability_matrix(struct subc *subc, int argc, char **argv)
{
	require_player_list();

	bool full_bus_names = false, csv = false;

	optind = 2;
	for (int opt; (opt = getopt(argc, argv, "bc")) != -1; ) {
		switch (opt) {
		case 'b':
			full_bus_names = true;
			break;
		case 'c':
			csv = true;
			break;
		default:        /* invalid option */
			exit_subc_invalid_usage(subc, NULL);
		}
	}

	/* Redirect output to “column” to format CSV to text. */
	if (!csv) {
		int fds[2];
		if (pipe(fds)) {
			perror("pipe");
			abort();
		}

		pid_t pid = fork();
		if (pid == 0) {
			close(0);
			if (dup2(fds[0], 0) < 0) {
				perror("dup2");
				abort();
			}
			close(fds[0]);
			close(fds[1]);
			execlp("column",
			       "column", "-t", "-s", ",",
			       NULL);
			perror("fork(column)");
			exit(1);
		} else if (pid > 0) {
			close(1);
			if (dup2(fds[1], 1) < 0) {
				perror("dup2");
				abort();
			}
			close(fds[0]);
			close(fds[1]);
		} else {
			perror("fork");
			abort();
		}
	}

	for (struct subc *subc = subcommands;
	     subc->name != NULL || subc->desc != NULL;
	     subc++) {
		if (subc->name == NULL)        continue;
		if (subc->does_not_use_player) continue;

		printf(",%s", subc->name);
	}
	putchar('\n');

	for (size_t pos = 0; pos < player_list.len; pos++) {
		player_bus_name = player_list.player_buses[pos];

		if (full_bus_names) {
			printf("%s,", player_bus_name);
		} else {
			const char *inst_name =
				bus_name_strip_common(player_bus_name);
			printf("%s,", inst_name);
		}

		for (struct subc *subc = subcommands;
		     subc->name != NULL || subc->desc != NULL;
		     subc++) {
			if (subc->name == NULL)        continue;
			if (subc->does_not_use_player) continue;

			subc->avail_status = SUBC_AVAIL_UNKNOWN;
			check_subc_availability(subc);

			static char const *status_chars[] = {
				[SUBC_AVAIL_UNKNOWN] = NULL,
				[SUBC_UNIMPLEMENTED] = "unimpl",
				[SUBC_UNAVAILABLE]   = "no",
				[SUBC_AVAILABLE]     = "yes",
				[SUBC_NO_PLAYER]     = NULL,
			};
			printf("%s", status_chars[subc->avail_status]);

			if (subc[1].name != NULL || subc[1].desc != NULL) {
				putchar(',');
			} else {
				putchar('\n');
			}
		}
	}

	return MPRIS2_EXIT_SUCCESS;
}

