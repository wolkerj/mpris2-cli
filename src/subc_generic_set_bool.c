/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"
#include "subc_util.h"

enum action
{
	SET_FALSE  = 0,
	SET_TRUE   = 1,
	TOGGLE     = 2,
};

static enum action parse_action(struct subc *, char const *);

int subc_generic_set_bool(struct subc *subc, int argc, char **argv)
{
	optind = 2;
	for (int opt; (opt = getopt(argc, argv, "")) != -1; ) {
		switch (opt) {
		case '?':
			exit_subc_invalid_usage(subc, NULL);
		}
	}

	if (optind == argc) {
		exit_subc_invalid_usage(subc, "New value required");
	}
	if (optind + 1 != argc) {
		exit_subc_invalid_usage(subc, "Too many positional arguments");
	}

	enum action action = parse_action(subc, argv[optind]);
	bool needs_previous_state = action == TOGGLE;

	require_dbus();

	GError *error = NULL;

	bool prev_state;
	if (needs_previous_state) {
		prev_state = dbus_get_bool_property(
			player_bus_name,
			"/org/mpris/MediaPlayer2",
			subj_interface(subc->dbus_subject),
			subc->dbus_action,
			G_DBUS_CALL_FLAGS_NONE,
			&error);
		report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
		              "Failed to get original D-Bus property state");
	}

	bool new_state;
	switch (action) {
	case SET_TRUE:
		new_state = true;
		break;
	case SET_FALSE:
		new_state = false;
		break;
	case TOGGLE:
		new_state = !prev_state;
		break;
	}

	dbus_set_bool_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(subc->dbus_subject),
		subc->dbus_action,
		new_state,
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to set D-Bus property");

	return MPRIS2_EXIT_SUCCESS;
}

static enum action parse_action(struct subc *subc, char const *act) 
{
	static char const *actions[] = {
		"\1" "true",
		"\1" "t",
		"\1" "y",
		"\1" "yes",
		"\1" "1",
		"\0" "false",
		"\0" "f",
		"\0" "n",
		"\0" "no",
		"\0" "0",
		"\2" "toggle",
		NULL
	};

	for (char const **action = actions; *action != NULL; action++) {
		char state = (*action)[0];
		char const *action_name = *action + 1;

		if (strcasecmp(action_name, act) == 0) {
			return state;
		}
	}

	exit_subc_invalid_usage(subc, "Invalid value");
}
