/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#ifndef MPRIS2CLI_EXIT_CODES_H
#define MPRIS2CLI_EXIT_CODES_H

enum mpris2_exit_codes
{
	MPRIS2_EXIT_SUCCESS            = 0,
	MPRIS2_EXIT_UNKNOWN_SUBCOMMAND = 1,
	MPRIS2_EXIT_NO_PLAYER          = 2,
	MPRIS2_EXIT_NOT_AVAILABLE      = 3,
	MPRIS2_EXIT_PLAYER_ERROR       = 4,
	MPRIS2_EXIT_UNIMPLEMENTED      = 5,
	MPRIS2_EXIT_DBUS_ERROR         = 6,
	MPRIS2_EXIT_INVALID_USAGE      = 7,
	MPRIS2_EXIT_UNSUPPORTED        = 8,
};

#endif	// MPRIS2CLI_EXIT_CODES_H
