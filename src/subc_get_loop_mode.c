/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"
#include "player_util.h"
#include "subc_util.h"

int subc_get_loop_mode(struct subc *subc, int argc, char **argv)
{
	require_dbus();

	enum loop_mode mode = get_loop_mode();

	static char const *names[] = {
		[LOOP_MODE_NONE]     = "Off",
		[LOOP_MODE_TRACK]    = "Track",
		[LOOP_MODE_PLAYLIST] = "Playlist",
		[LOOP_MODE_UNKNOWN]  = "Unknown",
	};

	if (mode == LOOP_MODE_UNSUPPORTED) {
		fputs("Loop mode information is not provided by the player\n",
		      stderr);
		exit(MPRIS2_EXIT_UNSUPPORTED);
	}

	puts(names[mode]);

	return MPRIS2_EXIT_SUCCESS;
}

