/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <assert.h>

#include <gio/gio.h>

#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"

void check_subc_availability(struct subc *subc)
{
	if (subc->run == NULL) {
		subc->avail_status = SUBC_UNIMPLEMENTED;
		return;
	}
	if (subc->does_not_use_player) {
		subc->avail_status = SUBC_NO_PLAYER;
		return;
	}

	subc->avail_status = SUBC_AVAILABLE;
	/* … or we prove the opposite. */

	/* When the subcommand depends on some D-Bus property, check for its
	 * availability. */
	if (subc->dbus_needs_prop != NULL) {
		require_dbus();
		assert(player_bus_name != NULL);

		GError *error = NULL;
		GVariant *result = dbus_get_property(
			player_bus_name,
			"/org/mpris/MediaPlayer2",
			subj_interface(subc->dbus_subject),
			subc->dbus_needs_prop,
			G_DBUS_CALL_FLAGS_NONE,
			&error);
		if (result != NULL) {
			g_variant_unref(result);
		}
		if (error != NULL && error->code == G_DBUS_ERROR_INVALID_ARGS) {
			g_error_free(error);
			subc->avail_status = SUBC_UNAVAILABLE;
			return;
		} else {
			report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
			              "Failed to check for proeprty presence");
		}
	}

	/* When a test D-Bus call is provided, perform it. */
	if (subc->dbus_test != NULL) {
		require_dbus();
		assert(player_bus_name != NULL);

		GError *error = NULL;
		bool available = dbus_get_bool_property(
			player_bus_name,
			"/org/mpris/MediaPlayer2",
			subj_interface(subc->dbus_subject),
			subc->dbus_test,
			G_DBUS_CALL_FLAGS_NONE,
			&error);
		report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
		              "Failed to execute availability test");

		if (!available) {
			subc->avail_status = SUBC_UNAVAILABLE;
			return;
		}
	}
}

bool subc_available(struct subc const *subc)
{
	assert(subc->avail_status != SUBC_AVAIL_UNKNOWN);
	return subc->avail_status >= SUBC_AVAILABLE;
}
