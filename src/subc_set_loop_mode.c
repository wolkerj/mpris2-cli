/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"
#include "player_util.h"
#include "subc_util.h"

static enum loop_mode next_mode(enum loop_mode);

int subc_set_loop_mode(struct subc *subc, int argc, char **argv)
{
	require_dbus();

	enum loop_mode target_mode;
	bool toggle = false;
	bool cycle = false;

	int argn = 2;

	if (argc < 3) {
		exit_subc_invalid_usage(
			subc,
			"Loop mode required.");
	}

	if (strcasecmp(argv[argn], "toggle") == 0) {
		toggle = true;
		argn++;
		if (argc != 4) {
			exit_subc_invalid_usage(
				subc,
				"Toggle requires one positional argument");
		}
	}
	if (strcasecmp(argv[argn], "cycle") == 0) {
		cycle = true;
		if (argc != 3) {
			exit_subc_invalid_usage(
				subc,
				"Cycle does not take any arguments");
		}
	} else if (strcasecmp(argv[argn], "none") == 0) {
		target_mode = LOOP_MODE_NONE;
	} else if (strcasecmp(argv[argn], "track") == 0) {
		target_mode = LOOP_MODE_TRACK;
	} else if (strcasecmp(argv[argn], "playlist") == 0) {
		target_mode = LOOP_MODE_PLAYLIST;
	} else if (argv[argn][0] == '-') {
		exit_subc_invalid_usage(
			subc,
			"This subcommand takes no options");
	} else {
		exit_subc_invalid_usage(subc, "Invalid cycle mode");
	}

	enum loop_mode current_mode;
	if (toggle || cycle) {
		current_mode = get_loop_mode();

		if (current_mode == LOOP_MODE_UNSUPPORTED) {
			fputs("Loop mode information is not provided by the player\n",
			      stderr);
			exit(MPRIS2_EXIT_UNSUPPORTED);
		}
	}

	if (cycle) {
		target_mode = next_mode(current_mode);
	}

	if (toggle) {
		if (current_mode != LOOP_MODE_NONE) {
			if (current_mode == target_mode) {
				target_mode = LOOP_MODE_NONE;
			}
		}
	}

	set_loop_mode(target_mode);

	return MPRIS2_EXIT_SUCCESS;
}

static enum loop_mode next_mode(enum loop_mode mode)
{
	static enum loop_mode const modes[] = {
		[LOOP_MODE_NONE]        = LOOP_MODE_PLAYLIST,
		[LOOP_MODE_PLAYLIST]    = LOOP_MODE_TRACK,
		[LOOP_MODE_TRACK]       = LOOP_MODE_NONE,
		[LOOP_MODE_UNSUPPORTED] = LOOP_MODE_NONE,
	};
	return modes[mode];
}
