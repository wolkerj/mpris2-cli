/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "player_util.h"

#include <math.h>
#include <stdint.h>
#include <string.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"

enum playback_status get_playback_status(void)
{
	GError *error = NULL;
	GVariant *result = dbus_get_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		"PlaybackStatus",
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to get playback status");
	enum playback_status status = PLAYBACK_UNKNOWN;
	char const *status_s = g_variant_get_string(result, NULL);

	if (strcmp(status_s, "Playing") == 0) {
		status = PLAYBACK_PLAYING;
	} else if (strcmp(status_s, "Paused") == 0) {
		status = PLAYBACK_PAUSED;
	} else if (strcmp(status_s, "Stopped") == 0) {
		status = PLAYBACK_STOPPED;
	}

	g_variant_unref(result);
	return status;
}

char const *playback_status_name(enum playback_status status)
{
	static char const *statuses[] = {
		[PLAYBACK_PLAYING] = "playing",
		[PLAYBACK_PAUSED]  = "paused",
		[PLAYBACK_STOPPED] = "stopped",
		[PLAYBACK_UNKNOWN] = "unknown",
	};
	return statuses[status];
}

char const *get_player_identity(void)
{
	GError *error = NULL;
	static GVariant *result = NULL;

	if (result != NULL) {
		g_variant_unref(result);
	}

	result = dbus_get_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_APP),
		"Identity",
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to get player identity");
	return g_variant_get_string(result, NULL);
}

unsigned long get_playback_pos(void)
{
	GError *error = NULL;
	unsigned long pos;

	GVariant *result = dbus_get_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		"Position",
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	if (error != NULL && error->code == G_DBUS_ERROR_NOT_SUPPORTED) {
		pos = ULONG_MAX;
		g_error_free(error);
	} else {
		report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
		              "Failed to get playback position");
		pos = g_variant_get_int64(result);
		g_variant_unref(result);
	}
	return pos;
}

double get_playback_rate_prop(enum playback_rate_prop prop)
{
	GError *error = NULL;

	static char const *rate_props[] = {
		[PLAYBACK_RATE_MIN]  = "MinimumRate",
		[PLAYBACK_RATE_CURR] = "Rate",
		[PLAYBACK_RATE_MAX]  = "MaximumRate",
	};

	double rate = dbus_get_double_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		rate_props[prop],
		G_DBUS_CALL_FLAGS_NONE,
		&error);

	if (error != NULL && error->code == G_DBUS_ERROR_NOT_SUPPORTED) {
		rate = 1.0f;
		g_error_free(error);
	} else {
		report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
		              "Failed to get rate prop");
	}
	return rate;
}

double get_playback_volume(void)
{
	GError *error = NULL;

	double volume = dbus_get_double_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		"Volume",
		G_DBUS_CALL_FLAGS_NONE,
		&error);

	if (error != NULL && error->code == G_DBUS_ERROR_NOT_SUPPORTED) {
		volume = NAN;
		g_error_free(error);
	} else {
		report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
		              "Failed to get volume prop");
	}
	return volume;
}

enum shuffle_status get_shuffle_status(void)
{
	GError *error = NULL;

	enum shuffle_status shuffle = dbus_get_bool_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		"Shuffle",
		G_DBUS_CALL_FLAGS_NONE,
		&error) ? SHUFFLE_YES : SHUFFLE_NO;

	if (error != NULL && (
		    error->code == G_DBUS_ERROR_NOT_SUPPORTED
		    || error->code == G_DBUS_ERROR_INVALID_ARGS)) {
		shuffle = SHUFFLE_UNSUPPORTED;
		g_error_free(error);
	} else {
		report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
		              "Failed to get shuffle prop");
	}
	return shuffle;
}

enum loop_mode get_loop_mode(void)
{
	GError *error = NULL;
	GVariant *result = dbus_get_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		"LoopStatus",
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	if (error != NULL && (
		    error->code == G_DBUS_ERROR_NOT_SUPPORTED
		    || error->code == G_DBUS_ERROR_INVALID_ARGS)) {
		g_error_free(error);
		return LOOP_MODE_UNSUPPORTED;
	}

	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to get playback status");
	enum loop_mode mode = LOOP_MODE_UNKNOWN;
	char const *mode_s = g_variant_get_string(result, NULL);

	if (strcmp(mode_s, "None") == 0) {
		mode = LOOP_MODE_NONE;
	} else if (strcmp(mode_s, "Track") == 0) {
		mode = LOOP_MODE_TRACK;
	} else if (strcmp(mode_s, "Playlist") == 0) {
		mode = LOOP_MODE_PLAYLIST;
	}

	g_variant_unref(result);
	return mode;
}

void set_loop_mode(enum loop_mode mode)
{
	static char const *names[] = {
		[LOOP_MODE_NONE]     = "None",
		[LOOP_MODE_TRACK]    = "Track",
		[LOOP_MODE_PLAYLIST] = "Playlist",
	};
	GError *error = NULL;
	dbus_set_string_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(SUBJ_PLAYER),
		"LoopStatus",
		names[mode],
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	report_gerror(error, MPRIS2_EXIT_UNSUPPORTED,
		"Setting loop status is not supported");
}
