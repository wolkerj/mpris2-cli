/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#ifndef MPRIS2CLI_BUS_NAME_UTIL_H
#define MPRIS2CLI_BUS_NAME_UTIL_H

#include <stdint.h>

enum action_subject
{
	SUBJ_APP,
	SUBJ_PLAYER,
};

const char *bus_name_strip_common(const char *);
size_t inst_name_player_name_len(const char *);

char const *subj_interface(enum action_subject);

#endif	// MPRIS2CLI_BUS_NAME_UTIL_H
