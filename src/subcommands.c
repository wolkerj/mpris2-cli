/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include <stdlib.h>

#include "subcommands.h"

int subc_help(struct subc *, int argc, char **argv);
int subc_players(struct subc *, int argc, char **argv);
int subc_availability_matrix(struct subc *, int argc, char **argv);
int subc_status(struct subc *, int argc, char **argv);
int subc_generic_invoke(struct subc *, int argc, char **argv);
int subc_generic_get_bool(struct subc *, int argc, char **argv);
int subc_generic_set_bool(struct subc *, int argc, char **argv);
int subc_get_loop_mode(struct subc *, int argc, char **argv);
int subc_set_loop_mode(struct subc *, int argc, char **argv);

static char const get_bool_usage[] = "[-j1]";
static char const get_bool_docs[] =
	"-j      Prints “true” or “false” instead of verbose message\n"
	"\n"
	"-1      Prints “1” or “0” instead of verbose message\n";

static char const set_bool_usage[] = "{<state>|toggle}";
static char const set_bool_docs[] =
	"1, true, yes, y\n"
	"      Enable the option.\n"
	"\n"
	"0, false, no, n\n"
	"      Disable the option\n"
	"\n"
	"toggle\n"
	"      Toggle the option\n";

struct subc subcommands[] = {
	{
		.desc = "General commands",
	},
	{
		.name  = "help",
		.desc  = "Show help message",
		.usage = "[-ar] [<subcommand>]",
		.docs  = "-a      Show availability of the subcommands.\n"
		         "        (This will interact with the selected player.)\n"
		         "\n"
		         "-r      Show only commands relevant to the player.\n"
		         "\n"
		         "<subcommand>\n"
		         "        Print verbose information about the subcommand.",

		.uses_available_on_others = true,
		.does_not_use_player = true,

		.run = subc_help,
	},
	{
		.name  = "players",
		.desc  = "Show list of available players",
		.usage = "[-i|-b]",
		.docs  = "-b      Show full D-Bus player bus names\n"
		         "\n"
		         "-i      Show instance names (intead of just players IDs)\n",

		.does_not_use_player = true,

		.run = subc_players,
	},
	{
		.name  = "availability-matrix",
		.desc  = "Show matrix of features supported by the running players",
		.usage = "[-c]",
		.docs  = "-b      Show full D-Bus player bus names\n"
		         "\n"
		         "-c      Output CSV\n",

		.does_not_use_player = true,

		.run = subc_availability_matrix,
	},
	{
		.desc = "Player control",
	},
	{
		.name  = "status",
		.desc  = "Print player status",
		.usage = "[-w]",
		.docs  = "-w[<interval>]\n"
		         "        Show the status periodically.\n"
		         "        When the interval (in seconds) is provided,\n"
		         "        the status is printed periodically. Use 0\n"
		         "        to show the status only when SIGUSR1 is\n"
		         "        is received.\n"
			 "\n"
		         "When -w is provided, the command will wait after\n"
		         "printing out the status for the provided time or\n"
			 "until SIGUSR1 is received.\n"
			 "\n"
		         "When -w is not provided, the command will print the\n"
		         "status and exit.\n",

		.run = subc_status,
	},
	{
		.name  = "play",
		.desc  = "Start playing the media",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "Play",
		.dbus_test    = "CanPlay",

		.error_message       = "Failed to start the playback",
		.unavailable_message = "Playback cannot be started",
	},	
	{
		.name  = "pause",
		.desc  = "Pause the playback",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "Pause",
		.dbus_test    = "CanPause",

		.error_message       = "Failed to pause the playback",
		.unavailable_message = "Playback cannot be paused",
	},
	{
		.name  = "toggle",
		.desc  = "Toggle pause state",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "PlayPause",
		.dbus_test    = "CanPause",

		.error_message       = "Failed to toggle pause",
		.unavailable_message = "Playback cannot be paused/toggled",
	},
	{
		.name  = "stop",
		.desc  = "Stop the playback",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "Stop",
		.dbus_test    = "CanControl",

		.error_message       = "Failed to stop the playback",
		.unavailable_message = "Playback cannot be stopped",
	},
	{
		.name  = "can-control",
		.desc  = "Query for playback control availability",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "CanControl",

		.true_message  = "Playback can be controlled.",
		.false_message = "Playback control is not available.",
	},
	{
		.name  = "can-pause",
		.desc  = "Query for “pause” control availability",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "CanPause",

		.true_message  = "Playback can be paused.",
		.false_message = "Pause control is not available.",
	},
	{
		.name  = "quit",
		.desc  = "Quit the player",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_APP,
		.dbus_action  = "Quit",
		.dbus_test    = "CanQuit",

		.error_message       = "Failed to quit the player",
		.unavailable_message = "Player cannot be quitted using MPRIS2",
	},
	{
		.name  = "can-quit",
		.desc  = "Check whether the player can be quit",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject = SUBJ_APP,
		.dbus_action  = "CanQuit",

		.true_message  = "Player can be quit.",
		.false_message = "It's not possible to quit using MPRIS2.",
	},
	{
		.name  = "next",
		.desc  = "Play the next track",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "Next",
		.dbus_test    = "CanGoNext",

		.error_message       = "Failed to change the track",
		.unavailable_message = "Next track is not available",
	},
	{
		.name  = "prev",
		.desc  = "Play the previous track",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "Previous",
		.dbus_test    = "CanGoPrevious",

		.error_message       = "Failed to change the track",
		.unavailable_message = "Previous track is not available",
	},
	{
		.name  = "can-go-next",
		.desc  = "Query for “next” control availability",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "CanGoNext",

		.true_message  = "You can go to the next track.",
		.false_message = "Next is not available.",
	},
	{
		.name  = "can-go-prev",
		.desc  = "Query for “prev” control availability",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject = SUBJ_PLAYER,
		.dbus_action  = "CanGoPrev",

		.true_message  = "You can go to the previous track.",
		.false_message = "Previous is not available.",
	},
	{
		.name  = "get-shuffle",
		.desc  = "Get shuffle state",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject    = SUBJ_PLAYER,
		.dbus_action     = "Shuffle",
		.dbus_needs_prop = "Shuffle",

		.true_message  = "Shuffle: on",
		.false_message = "Shuffle: off",

		.error_message = "Failed to get shuffle status",
	},
	{
		.name  = "set-shuffle",
		.desc  = "Set shuffle state",
		.usage = set_bool_usage,
		.docs  = set_bool_docs,

		.run = subc_generic_set_bool,
		.dbus_subject    = SUBJ_PLAYER,
		.dbus_action     = "Shuffle",
		.dbus_test       = "CanControl",
		.dbus_needs_prop = "Shuffle",

		.error_message       = "Failed to set shuffle mode",
		.unavailable_message = "Shuffle mode cannot be changed",
	},
	{
		.name  = "get-loop",
		.desc  = "Get loop mode",

		.run = subc_get_loop_mode,
		.dbus_subject    = SUBJ_PLAYER,
		.dbus_needs_prop = "LoopStatus",

		.unavailable_message = "Loop mode info is not supported",
	},
	{
		.name  = "set-loop",
		.desc  = "Set loop mode",
		.usage = "<mode>",
		.docs  = "<mode> can be one of these:\n"
		         "\n"
			 "none\n"
			 "        Disable looped play.\n"
			 "\n"
			 "track\n"
			 "        Repeat one track.\n"
			 "\n"
			 "playlist\n"
			 "        Repeat multiple tracks.\n"
			 "\n"
			 "toggle track\n"
			 "toggle playlist\n"
			 "        Cycle between “none” and one other mode.\n"
			 "\n"
			 "cycle\n"
		         "        Cycle all available modes.\n",

		.run = subc_set_loop_mode,
		.dbus_subject    = SUBJ_PLAYER,
		.dbus_needs_prop = "LoopStatus",

		.unavailable_message = "Loop mode info is not supported",
	},
	{
		.desc = "Window control",
	},
	{
		.name  = "raise",
		.desc  = "Raise the player window",

		.run = subc_generic_invoke,
		.dbus_subject = SUBJ_APP,
		.dbus_action  = "Raise",
		.dbus_test    = "CanRaise",

		.error_message       = "Failed to raise the player",
		.unavailable_message = "Player cannot be raised using MPRIS2",
	},
	{
		.name  = "can-raise",
		.desc  = "Check whether the player can be raised",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject = SUBJ_APP,
		.dbus_action  = "CanRaise",

		.true_message  = "Player window can be raised.",
		.false_message = "It's not possible to raise the window.",
	},
	{
		.name  = "get-fullscreen",
		.desc  = "Get fullscreen state",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject    = SUBJ_APP,
		.dbus_action     = "Fullscreen",
		.dbus_needs_prop = "Fullscreen",

		.true_message  = "Fullscreen",
		.false_message = "Windowed",

		.error_message = "Failed to get fullscreen status",
	},
	{
		.name  = "set-fullscreen",
		.desc  = "Set fullscreen state",
		.usage = set_bool_usage,
		.docs  = set_bool_docs,

		.run = subc_generic_set_bool,
		.dbus_subject    = SUBJ_APP,
		.dbus_action     = "Fullscreen",
		.dbus_test       = "CanSetFullscreen",
		.dbus_needs_prop = "CanSetFullscreen",

		.error_message       = "Failed to set fullscreen status",
		.unavailable_message = "Fullscreen mode cannot be changed",
	},
	{
		.name  = "can-set-fullscreen",
		.desc  = "Check whether the fullscreen state can be controlled",
		.usage = get_bool_usage,
		.docs  = get_bool_docs,

		.run = subc_generic_get_bool,
		.dbus_subject    = SUBJ_APP,
		.dbus_action     = "CanSetFullscreen",
		.dbus_needs_prop = "CanSetFullscreen",

		.true_message        = "Fullscreen control is available.",
		.false_message       = "Cannot control fullscreen mode.",
		.unavailable_message = "Fullscreen mode control is not implemented by this player",
	},
	{ 0 }
};
