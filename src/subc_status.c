/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"
#include "player_util.h"
#include "subc_util.h"

#define DEFAULT_WATCH_TIME 500

static bool usr1_recvd = false;

static void show_status(void);
static void msleep_or_usr1(long);
static void handle_usr1(int);

int subc_status(struct subc *subc, int argc, char **argv)
{
	int watch_millis = -1;

	optind = 2;
	for (int opt; (opt = getopt(argc, argv, "w::")) != -1; ) {
		switch (opt) {
		case 'w':
			float val;
			if (optarg == NULL) {
				watch_millis = DEFAULT_WATCH_TIME;
			} else if (sscanf(optarg, "%f", &val) > 0
			           && (val == 0.0 || isnormal(val))) {
				watch_millis = val * 1000.f;
			} else {
				exit_subc_invalid_usage(subc,
				                        "Invalid interval");
			}
			break;
		case '?':
			exit_subc_invalid_usage(subc, NULL);
		}
	}

	require_dbus();

	do {
		show_status();
		if (watch_millis >= 0) {
			msleep_or_usr1(watch_millis);
		}
	} while (watch_millis >= 0);

	return MPRIS2_EXIT_SUCCESS;
}

static void show_status(void)
{
	GError *error = NULL;

	char const *identity = get_player_identity();

	enum playback_status status = get_playback_status();
	char const *status_name = playback_status_name(status);

	unsigned long int playback_pos = get_playback_pos();
	unsigned long int playback_secs = playback_pos / 1000000;
	unsigned hrs  = playback_secs / 3600;
	unsigned mins = (playback_secs % 3600) / 60;
	unsigned secs = playback_secs % 60;

	double rate = get_playback_rate_prop(PLAYBACK_RATE_CURR);
	double min_rate = get_playback_rate_prop(PLAYBACK_RATE_MIN);
	double max_rate = get_playback_rate_prop(PLAYBACK_RATE_MAX);

	double volume = get_playback_volume();

	enum shuffle_status shuffle = get_shuffle_status();
	enum loop_mode loop = get_loop_mode();
	static char const *loop_mode_names[] = {
		[LOOP_MODE_NONE]     = "off",
		[LOOP_MODE_TRACK]    = "track",
		[LOOP_MODE_PLAYLIST] = "playlist",
		[LOOP_MODE_UNKNOWN]  = "unknown",
	};
	char const *loop_mode_name = loop_mode_names[loop];

	printf("[%s] ", status_name);
	puts(identity);

	if (playback_pos == ULONG_MAX) {
		printf("unknown position");
	} else {
		printf("%02d:%02d:%02d",
		       hrs, mins, secs);
	}
	if (min_rate != max_rate || rate != 1.0f) {
		printf(" @ %.2f×", rate);
		printf(" (of %.2f–%.2f×)", min_rate, max_rate);
	}
	putchar('\n');
	if (!isnan(volume)) {
		printf("volume: ◢ %.0f%%\n", volume * 100.f);
	} else {
		puts("volume: unknown");
	}
	if (shuffle != SHUFFLE_UNSUPPORTED || loop != LOOP_MODE_UNSUPPORTED) {
		if (shuffle != SHUFFLE_UNSUPPORTED) {
			printf("shuffle: %s  ",
			       shuffle ? "on" : "off");
		}
		if (loop != LOOP_MODE_UNSUPPORTED) {
			printf("loop: %s  ", loop_mode_name);
		}
		putchar('\n');
	}
}

/* Zero means infinite sleep. (See setitimer(3).) */
static void msleep_or_usr1(long millis)
{
	sigset_t empty_sigs;

	sigemptyset(&empty_sigs);

	struct sigaction act = {
		.sa_handler = handle_usr1,
		.sa_mask    = empty_sigs,
		.sa_flags   = SA_RESTART,
	};
	if (sigaction(SIGUSR1, &act, NULL)) {
		perror("sigaction(USR1)");
		abort();
	}
	if (sigaction(SIGALRM, &act, NULL)) {
		perror("sigaction(ALRM)");
		abort();
	}

	struct timeval time = {
		.tv_sec  = millis / 1000,
		.tv_usec = (millis % 1000) * 1000,
	};
	struct itimerval val = {
		.it_interval = { 0 },
		.it_value    = time,
	};
	if (setitimer(ITIMER_REAL, &val, NULL)) {
		perror("setitimer");
		abort();
	}

	usr1_recvd = false;
	while (!usr1_recvd) {
		if (sigsuspend(&empty_sigs)) {
			if (errno != EINTR) {
				perror("sugsuspend");
				abort();
			}
		}
	}
}

static void handle_usr1(int sig)
{
	usr1_recvd = true;
}
