/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "dbus_util.h"
#include "subcommands.h"
#include "subc_util.h"

static void print_usage(void);
static void print_subc_line(struct subc *, bool print_availability);

static void print_subcommand_help_by_name(struct subc *, char const *);
static void print_subcommand_help(struct subc *);

int subc_help(struct subc *subc,
              int argc, char **argv)
{
	bool print_availability  = false;
	bool print_only_relevant = false;

	optind = 2;
	for (int opt; (opt = getopt(argc, argv, "ar")) != -1; ) {
		switch (opt) {
		case 'a':
			print_availability = true;
			break;
		case 'r':
			print_only_relevant = true;
			break;
		case '?':
			exit_subc_invalid_usage(subc, NULL);
		}
	}

	bool needs_availability_status =
		print_only_relevant || print_availability;
	if (needs_availability_status && player_bus_name == NULL) {
		use_first_player();
	}

	if (optind < argc - 1) {
		exit_subc_invalid_usage(subc, "Too many arguments");
	} else if (optind < argc) {
		print_subcommand_help_by_name(subc, argv[optind]);
		return EXIT_SUCCESS;
	}

	print_usage();
	puts("");

	puts("This program accepts variety of subcommands. Specify one as the\n"
	     "first positional argument.\n"
	     "\n"
	     "Program-wide options:\n"
	     "\n"
	     "        Specify these options before the subcommand name.\n"
	     "\n"
	     "        -p <player_id>[.<instance_id>]\n"
	     "                Specify player to control.\n"
	     "\n"
	     "        -P <player_bus_name>\n"
	     "                Specify player to control using full D-Bus\n"
	     "                bus name.");

	if (needs_availability_status) {
		printf("\n"
		       "| NOTE\n"
		       "|\n"
		       "| The subcommand availability information below\n"
		       "| applies to player with bus name %s.\n",
		       player_bus_name);
	}
	if (print_only_relevant) {
		puts("|\n"
		     "| Only subcommands usable with the player are listed.");
	}
	if (needs_availability_status) {
		puts("|\n"
		     "| To list all supported subcommands without using\n"
		     "| information provided by the player, run\n"
		     "| command „mpris2 help“ without any additional\n"
		     "| arguments.");
	}

	bool has_unimplemented_subcs = false;

	for (struct subc *subc = &subcommands[0];
	     is_valid_subc(subc);
	     subc++) {
		if (subc->name == NULL) { /* This is a header. */
			printf("\n%s:\n\n", subc->desc);
			continue;
		} else {                  /* This is a subcommand. */
			if (needs_availability_status
			    && subc->avail_status == SUBC_AVAIL_UNKNOWN) {
				check_subc_availability(subc);
			}
			if (subc->run == NULL) {
				has_unimplemented_subcs = true;
			}
			if (print_only_relevant && !subc_available(subc)) {
				continue;
			}
			print_subc_line(subc, print_availability);
		}
	}

	if (print_availability) {
		puts("\n"
		     "The subcommands marked with a character can have their\n"
		     "availability restricted depending on the player and its\n"
		     "state. The characters are:\n"
		     "\n"
		     "        *  subcommand available\n"
		     "        -  subcommand not supported or not available");
		if (has_unimplemented_subcs) {
			puts("        U  subcommand not implemented");
		}
	}

	if (has_unimplemented_subcs) {
		puts("\n"
		     "Some of the subcommands are not yet implemented. These\n"
		     "subcommands will report an error when executed.");
	}
	if (!print_availability) {
		puts("\n"
		     "Run “mpris2 help -a” to show subcommand availability.");
	}
	puts("\n"
	     "Run “mpris2 help <subcommand>” to show details about usage of\n"
	     "the given subcommand.\n"
	     "\n"
	     "This tool fully depends on features provided by the media\n"
	     "player. Not all players expose all available features using\n"
	     "the MPRIS2 interface.");

	return 0;
}

void print_subc_line(struct subc *subc, bool print_availability)
{
	static char const avail_chars[] = {
		[SUBC_AVAIL_UNKNOWN] = '?',
		[SUBC_AVAILABLE]     = '*',
		[SUBC_UNIMPLEMENTED] = 'U',
		[SUBC_UNAVAILABLE]   = '-',
		[SUBC_NO_PLAYER]     = ' ',
	};

	int column = 8;

	printf("        %s", subc->name);
	column += strlen(subc->name);

	if (subc->usage != NULL) {
		printf(" %s", subc->usage);
		column += 1 + strlen(subc->usage);
	}

	if (subc->desc != NULL) {
		if (column > 31) {
			column = 0;
			putchar('\n');
		}
		while (column++ < 32) {
			putchar(' ');
		}
		if (print_availability) {
			putchar(avail_chars[subc->avail_status]);
			putchar(' ');
		}
		puts(subc->desc);
	} else {
		putchar('\n');
	}
}

void print_usage(void)
{
	puts("mpris2 -- Media player control interface");
	puts("Usage:\n"
	     "\tmpris2 [-p <player>[.<instance>]|-P <bus>] <subcommand> \\\n"
	     "\t       [args...]");
}

void print_subcommand_help_by_name(struct subc *help_subc, char const *name)
{
	for (struct subc *subc = &subcommands[0];
	     is_valid_subc(subc);
	     subc++) {
		if (subc->name != NULL && strcmp(subc->name, name) == 0) {
			print_subcommand_help(subc);
			return;
		}
	}
	exit_subc_invalid_usage(help_subc, "Unknown subcommand");
}

void print_subcommand_help(struct subc *subc)
{
	printf("mpris2 %s -- %s", subc->name, subc->desc);
	printf("\n"
	       "Usage:\n"
	       "\tmpris2 %s",
	       subc->name);
	if (subc->usage != NULL) {
		printf(" %s", subc->usage);
	}
	if (subc->docs != NULL) {
		printf("\n\nOptions:\n        ");
		for (char const *ch = subc->docs; *ch != '\0'; ch++) {
			if (ch[0] == '\n' && ch[1] != '\0' && ch[1] != '\n') {
				printf("\n        ");
			} else if (ch[0] != '\n' || ch[1] != '\0') {
				putchar(*ch);
			}
		}
	}
	putchar('\n');
}
