/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "bus_name_util.h"

const char *bus_name_strip_common(const char *bus_name)
{
	static char const bus_name_prefix[] = "org.mpris.MediaPlayer2.";
	static size_t bus_name_prefix_len = sizeof(bus_name_prefix) - 1;

	assert(strncmp(bus_name_prefix, bus_name, bus_name_prefix_len) == 0);

	return bus_name + bus_name_prefix_len;
}

size_t inst_name_player_name_len(const char *instance_name)
{
	return strchrnul(instance_name, '.') - instance_name;
}

char const *subj_interface(enum action_subject subj)
{
	switch (subj) {
	case SUBJ_APP:
		return "org.mpris.MediaPlayer2";
	case SUBJ_PLAYER:
		return "org.mpris.MediaPlayer2.Player";
	}
}
