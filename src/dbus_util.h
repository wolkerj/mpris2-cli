/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#ifndef MPRIS2CLI_DBUS_UTIL_H
#define MPRIS2CLI_DBUS_UTIL_H

#include <stdbool.h>
#include <stdint.h>

#include <gio/gio.h>

extern GDBusConnection *dbus_conn;

void require_dbus(void);

extern struct player_list
{
	char const **player_buses;
	size_t len;
} player_list;

/**
 * Acquire of all available media players and their instances.
 */
void require_player_list(void);

extern char const *player_bus_name;

void set_player_id(char const *player_id);
void use_first_player(void);

GVariant *dbus_get_property(char const *bus_name,
                            char const *object_name,
                            char const *interface_name,
                            char const *property_name,
                            GDBusCallFlags flags,
                            GError **error);

bool dbus_get_bool_property(char const *bus_name,
                            char const *object_name,
                            char const *interface_name,
                            char const *property_name,
                            GDBusCallFlags flags,
                            GError **error);

void dbus_set_bool_property(char const *bus_name,
                            char const *object_name,
                            char const *interface_name,
                            char const *property_name,
                            bool new_value,
                            GDBusCallFlags flags,
                            GError **error);

double dbus_get_double_property(char const *bus_name,
                                char const *object_name,
                                char const *interface_name,
                                char const *property_name,
                                GDBusCallFlags flags,
                                GError **error);

void dbus_set_string_property(char const *bus_name,
                              char const *object_name,
                              char const *interface_name,
                              char const *property_name,
                              char const *new_value,
                              GDBusCallFlags flags,
                              GError **error);

#endif	// MPRIS2CLI_DBUS_UTIL_H
