/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#ifndef MPRIS2CLI_SUBCOMMANDS_H
#define MPRIS2CLI_SUBCOMMANDS_H

#include <stdbool.h>
#include <stdlib.h>

#include "bus_name_util.h"

struct subc;

typedef int (subc_run_t)(struct subc *,
                         int argc, char **argv);

enum subc_availability
{
	/* The order of the fields is important. The first “available” state
	 * must be SUBC_AVAILABLE and no “unavailable” states must come after
	 * it. */

	/* Default value. */
	SUBC_AVAIL_UNKNOWN = 0,

	SUBC_UNIMPLEMENTED,
	SUBC_UNAVAILABLE,

	SUBC_AVAILABLE,
	SUBC_NO_PLAYER,
};

bool subc_available(struct subc const *);

struct subc
{
	char const *name;
	char const *usage, *desc, *docs;

	enum subc_availability avail_status;
	bool uses_available_on_others;
	bool does_not_use_player;

	subc_run_t     *run;

	/* Fields for use only by some commands: */

	enum action_subject dbus_subject;
	char const         *dbus_action;
	char const         *dbus_test;
	char const         *dbus_needs_prop;

	char const *true_message, *false_message;
	char const *unavailable_message, *error_message;
};


extern struct subc subcommands[];

static inline bool is_valid_subc(struct subc const *subc)
{
	return subc->name != NULL || subc->desc != NULL;
}

void check_subc_availability(struct subc *);

#endif	// MPRIS2CLI_SUBCOMMANDS_H
