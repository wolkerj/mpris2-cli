/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#ifndef MPRIS2CLI_PLAYER_UTIL_H
#define MPRIS2CLI_PLAYER_UTIL_H

enum playback_status
{
	PLAYBACK_STOPPED,
	PLAYBACK_PLAYING,
	PLAYBACK_PAUSED,
	PLAYBACK_UNKNOWN,
};

enum playback_status get_playback_status(void);
char const *playback_status_name(enum playback_status);

char const *get_player_identity(void);

unsigned long get_playback_pos(void);

enum playback_rate_prop
{
	PLAYBACK_RATE_MIN,
	PLAYBACK_RATE_CURR,
	PLAYBACK_RATE_MAX,
};

double get_playback_rate_prop(enum playback_rate_prop);

double get_playback_volume(void);

enum shuffle_status
{
	SHUFFLE_NO,
	SHUFFLE_YES,
	SHUFFLE_UNSUPPORTED,
};

enum shuffle_status get_shuffle_status(void);

enum loop_mode
{
	LOOP_MODE_NONE,
	LOOP_MODE_TRACK,
	LOOP_MODE_PLAYLIST,
	LOOP_MODE_UNSUPPORTED,
	LOOP_MODE_UNKNOWN,
};

enum loop_mode get_loop_mode(void);
void set_loop_mode(enum loop_mode);

#endif	// MPRIS2CLI_PLAYER_UTIL_H
