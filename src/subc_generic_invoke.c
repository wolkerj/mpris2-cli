/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdio.h>
#include <stdint.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"
#include "subc_util.h"

int subc_generic_invoke(struct subc *subc, int argc, char **argv)
{
	require_dbus();

	GError *error = NULL;
	GVariant *reply = g_dbus_connection_call_sync(
		dbus_conn,
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(subc->dbus_subject),
		subc->dbus_action,
		NULL,
		/* reply type */ NULL,
		G_DBUS_CALL_FLAGS_NONE,
		/* timeout */ -1 /* default */,
		NULL, &error);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to execute D-Bus method");

	g_free(reply);

	return MPRIS2_EXIT_SUCCESS;
}
