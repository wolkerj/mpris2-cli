/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <gio/gio.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "error.h"
#include "exit_codes.h"
#include "subc_util.h"

static void print_human(struct subc const *, bool);
static void print_01(struct subc const *, bool);
static void print_json(struct subc const *, bool);

int subc_generic_get_bool(struct subc *subc, int argc, char **argv)
{
	void (*print_fn)(struct subc const *, bool) = print_human;

	optind = 2;
	for (int opt; (opt = getopt(argc, argv, "j1")) != -1; ) {
		switch (opt) {
		case 'j':
			print_fn = print_json;
			break;
		case '1':
			print_fn = print_01;
			break;
		case '?':
			exit_subc_invalid_usage(subc, NULL);
		}
	}

	require_dbus();

	GError *error = NULL;
	bool result = dbus_get_bool_property(
		player_bus_name,
		"/org/mpris/MediaPlayer2",
		subj_interface(subc->dbus_subject),
		subc->dbus_action,
		G_DBUS_CALL_FLAGS_NONE,
		&error);
	report_gerror(error, MPRIS2_EXIT_DBUS_ERROR,
	              "Failed to get D-Bus property");

	print_fn(subc, result);

	return MPRIS2_EXIT_SUCCESS;
}

static void print_human(struct subc const *subc, bool state)
{
	puts(state
	     ? subc->true_message
	     : subc->false_message);
}
		
static void print_01(struct subc const *, bool state)
{
	puts(state ? "1" : "0");
}

static void print_json(struct subc const *, bool state)
{
	puts(state ? "true" : "false");
}

