/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

#include "subcommands.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "bus_name_util.h"
#include "dbus_util.h"
#include "exit_codes.h"
#include "subc_util.h"

int subc_players(struct subc *subc, int argc, char **argv)
{
	require_player_list();

	bool full_bus_names = false, show_instances = false;

	optind = 2;
	for (int opt; (opt = getopt(argc, argv, "bi")) != -1; ) {
		switch (opt) {
		case 'b':
			full_bus_names = true;
			break;
		case 'i':
			show_instances = true;
			break;
		default:        /* invalid option */
			exit_subc_invalid_usage(subc, NULL);
		}
	}
	
	if (full_bus_names && show_instances) {
		exit_subc_invalid_usage(
			subc,
			"Cannot print more and less information at once");
	}

	const char *last_inst_name = "";
	size_t last_player_n_len = 0;

	for (size_t pos = 0; pos < player_list.len; pos++) {
		const char *bus_name = player_list.player_buses[pos];
		if (full_bus_names) {
			puts(bus_name);
		} else {
			const char *inst_name = bus_name_strip_common(bus_name);
			if (show_instances) {
				puts(inst_name);
			} else {
				int player_name_len =
					inst_name_player_name_len(inst_name);
				if (last_player_n_len == player_name_len
				    && strncmp(last_inst_name, inst_name,
				               last_player_n_len) == 0) {
					continue;
				}
				printf("%.*s\n", player_name_len, inst_name);

				last_inst_name = inst_name;
				last_player_n_len = player_name_len;
			}
		}
	}

	return MPRIS2_EXIT_SUCCESS;
}
