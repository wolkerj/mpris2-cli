/*
 *  Copyright (c) 2023 Jiří Wolker <projekty@jwo.cz>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA.
 */

/*
 * mpris2-cli -- Command-line interface for MPRIS2 media players.
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <gio/gio.h>

#include "dbus_util.h"
#include "exit_codes.h"
#include "subc_util.h"
#include "subcommands.h"

static int exec_subc(struct subc *subc, int argc, char **argv);
static void require_no_player_bus(void);

int main(int argc, char **argv)
{
	char *subcommand_name = NULL;

	/* Parse common arguments. */
	for (int opt; (opt = getopt(argc, argv, "+p:P:")) != -1; ) {
		switch (opt) {
		case 'p':
			require_no_player_bus();
			set_player_id(optarg);
			break;
		case 'P':
			require_no_player_bus();
			player_bus_name = optarg;
			break;
		case '?':
			fprintf(stderr,
			        "mpris2: Invalid usage. Run “%s help” for help.\n",
			        argv[0]);
			exit(MPRIS2_EXIT_INVALID_USAGE);
		}
	}
	/* Remove the common arguments. */
	memmove(argv + 1, argv + optind, sizeof(char *) * (argc - optind));
	argc -= optind - 1;
	optind = 1;

	if (argc > 1) {
		subcommand_name = argv[1];
	} else {
		subcommand_name = "help";
	}

	/* Find the appropriate subcommand. */
	for (struct subc *subc = subcommands;
	     is_valid_subc(subc);
	     subc++) {
		if (subc->name != NULL
		    && strcmp(subc->name, subcommand_name) == 0) {
			return exec_subc(subc, argc, argv);
		}
	}

	fprintf(stderr,
	        "mpris2: Subcommand “%s” not found\n"
	        "        Try “%s help” to get help.\n",
	        subcommand_name, argv[0]);
	return MPRIS2_EXIT_UNKNOWN_SUBCOMMAND;
}

int exec_subc(struct subc *subc, int argc, char **argv)
{
	if (!subc->does_not_use_player && player_bus_name == NULL) {
		/* Try to use the first found player. */
		use_first_player();
	}

	if (subc->run == NULL) {
		fprintf(stderr,
		        "mpris2: Subcommand “%s” is not yet implemented.\n",
		        subc->name);
		return MPRIS2_EXIT_UNIMPLEMENTED;
	}

	if (subc->usage == NULL && argc > 2) {
		exit_subc_invalid_usage(
			subc, "This subcommand does not take arguments");
	}

	check_subc_availability(subc);
	if (!subc_available(subc)) {
		if (subc->unavailable_message != NULL) {
			fprintf(stderr, "%s\n", subc->unavailable_message);
		} else {
			fprintf(stderr,
			        "mpris2: Subcommand “%s” is not available for this player.\n",
			        subc->name);
		}
		return MPRIS2_EXIT_UNSUPPORTED;
	}

	return subc->run(subc, argc, argv);
}

void require_no_player_bus(void)
{
	if (player_bus_name != NULL) {
		fputs("mpris2: Player can be specified at most once.\n",
		      stderr);
		exit(MPRIS2_EXIT_INVALID_USAGE);
	}
}
